<?php

namespace ADW\CommonBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class AdvancedPublicationTrait
 *
 * @package ADW\CommonBundle\Model
 * @author Artur Vesker
 */
trait AdvancedPublicationTrait
{

    use PublicationTrait;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $publicationStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $publicationEndDate;

    /**
     * @return \DateTime
     */
    public function getPublicationStartDate()
    {
        return $this->publicationStartDate;
    }

    /**
     * @param \DateTime $publicationStartDate
     * @return self
     */
    public function setPublicationStartDate($publicationStartDate)
    {
        $this->publicationStartDate = $publicationStartDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublicationEndDate()
    {
        return $this->publicationEndDate;
    }

    /**
     * @param \DateTime $publicationEndDate
     * @return self
     */
    public function setPublicationEndDate($publicationEndDate)
    {
        $this->publicationEndDate = $publicationEndDate;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isPublished($considerDates = false)
    {
        if (!$considerDates) {
            return $this->published;
        }

        $now = new \DateTime();
        return $this->published && ($this->publicationStartDate > $now) && ($this->publicationEndDate < $now);
    }
}