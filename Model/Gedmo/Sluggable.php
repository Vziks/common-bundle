<?php

namespace ADW\CommonBundle\Model\Gedmo;

use Gedmo\Mapping\Annotation as Gedmo;

trait Sluggable
{
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     *
     * @Gedmo\Slug(fields={"title"})
     */
    private $slug;

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }
}