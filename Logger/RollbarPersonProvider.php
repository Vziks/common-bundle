<?php

namespace ADW\CommonBundle\Logger;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class RollbarPersonProvider
 *
 * @author Artur Vesker
 */
class RollbarPersonProvider
{

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * RollbarPersonProvider constructor.
     *
     * @param $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array|null
     */
    public function __invoke()
    {
        if (!$token = $this->tokenStorage->getToken()) {
            return null;
        }

        $user = $token->getUser();

        if (!($user instanceof UserInterface)) {
            return null;
        }

        $data = [
            'username' => $user->getUsername()
        ];

        if (method_exists($user, 'getId')) {
            $data['id'] = $user->getId();
        } else {
            $data['id'] = $data['username'];
        }

        if (method_exists($user, 'getEmail')) {
            $data['email'] = $user->getEmail();
        }

        return $data;
    }

}