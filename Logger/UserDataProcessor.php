<?php

namespace ADW\CommonBundle\Logger;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserDataProcessor.
 *
 * @author Artur Vesker
 */
class UserDataProcessor
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param array $record
     *
     * @return array
     */
    public function processRecord(array $record)
    {
        if (!$token = $this->tokenStorage->getToken()) {
            return $record;
        }

        if (!$user = $token->getUser()) {
            return $record;
        }

        if (!$user instanceof UserInterface) {
            return $record;
        }

        $record['context']['user']['username'] = $user->getUsername();

        if (method_exists($user, 'getId')) {
            $record['context']['user']['id'] = $user->getId();
        }

        return $record;
    }
}
