<?php

namespace ADW\CommonBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AutocompleteType extends AbstractType
{
    private $em;

    /**
     * AutocompleteType constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add($builder->create('term', 'text', array(
                'label' => false,
                'attr'  => array(
                    'class' => 'term',
                ),
            )))
            ->add($builder->create('entity_id', 'hidden', array(
                'attr' => array('class' => 'entity_id'),
            )));

        $builder->addModelTransformer(new CallbackTransformer(
            function ($entity) use ($options) {
                if (null === $entity) {
                    return array(
                        'term'      => '',
                        'entity_id' => '',
                    );
                }

                $termGetter = 'get' . ucfirst($options['target_entity_term_field']);
                $idGetter   = 'get' . ucfirst($options['target_entity_id_field']);

                return array(
                    'term'      => $entity->$termGetter(),
                    'entity_id' => $entity->$idGetter(),
                );
            },
            function ($values) use ($options) {
                if (!$values['entity_id']) {
                    if (true == $options['allow_add']) {
                        $entity = new $options['target_entity_class']();

                        $setter = 'set' . ucfirst($options['target_entity_term_field']);
                        $entity->$setter($values['term']);
                        return $entity;
                    } else {
                        throw new \Exception('Adding new objects is forbidden by \"\'allow_add\' => false\".');
                    }
                }

                if (!isset($options['target_entity_class'])) {
                    throw new \Exception('Entity class is not defined');
                }

                $finder = 'findOneBy' . ucfirst($options['target_entity_id_field']);

                $product = $this->em
                    ->getRepository($options['target_entity_class'])
                    ->$finder($values['entity_id'])
                ;

                if (null === $product) {
                    throw new TransformationFailedException(sprintf(
                        'Entity with "%s" "%s" does not exist!',
                        $options['target_entity_id_field'],
                        $values['entity_id']
                    ));
                }

                return $product;
            }
        ))
        ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars['target_entity_class'] = $options['target_entity_class'];
        $view->vars['target_entity_id_field'] = $options['target_entity_id_field'];
        $view->vars['target_entity_term_field'] = $options['target_entity_term_field'];
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('target_entity_class', null);
        $resolver->setDefault('target_entity_id_field', 'id');
        $resolver->setDefault('target_entity_term_field', 'title');
        $resolver->setDefault('allow_add', false);
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'common_autocomplete';
    }
}
