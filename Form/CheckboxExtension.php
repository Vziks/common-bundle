<?php

namespace ADW\CommonBundle\Form;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


/**
 * Class CheckboxExtension
 *
 * @package ADW\CommonBundle\Form
 * @author Artur Vesker
 */
class CheckboxExtension extends AbstractTypeExtension
{

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event){

        });
    }

    /**
     * @inheritdoc
     */
    public function getExtendedType()
    {
        return CheckboxType::class;
    }


}