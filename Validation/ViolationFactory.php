<?php

namespace ADW\CommonBundle\Validation;
use ADW\CommonBundle\Exception\ValidationViolationException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class ViolationFactory
 *
 * @author Artur Vesker
 */
class ViolationFactory
{

    /**
     * @param $message
     * @param null $path
     * @param null $invalidValue
     * @return ConstraintViolationList
     */
    public static function one($message, $path = null, $invalidValue = null)
    {
        return new ConstraintViolationList([
            new ConstraintViolation($message, null, [], null, $path, $invalidValue)
        ]);
    }

}