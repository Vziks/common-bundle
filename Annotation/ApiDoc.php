<?php

namespace ADW\CommonBundle\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class ApiDoc.
 *
 * @author Artur Vesker
 *
 * @Annotation
 * @Target({"METHOD"})
 */
class ApiDoc extends \Nelmio\ApiDocBundle\Annotation\ApiDoc
{
    /**
     * @var string
     */
    protected $routeName;

    /**
     * @return mixed
     */
    public function getRouteName()
    {
        return $this->routeName;
    }

    /**
     * @param mixed $routeName
     *
     * @return self
     */
    public function setRouteName($routeName)
    {
        $this->routeName = $routeName;

        return $this;
    }

    public function toArray()
    {
        $route = $this->getRoute();

        if ($route->getOption('expose') && $this->routeName) {
            $this->setDocumentation($this->getDocumentation()."<br><h4>Route</h4> {$this->routeName}");
        }

        return parent::toArray();
    }
}
