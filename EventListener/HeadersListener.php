<?php
/**
 * Created by PhpStorm.
 * User: Aleksey Kolyadin
 * Date: 08.02.2016
 * Time: 17:34
 */

namespace ADW\CommonBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Class HeadersListener
 *
 * @package ADW\CommonBundle\EventListener
 */
class HeadersListener
{

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @param array $headers
     */
    public function __construct(array $headers = [])
    {
        $this->headers = $headers;
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onResponse(FilterResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $event->getResponse()->headers->add($this->headers);
    }

}