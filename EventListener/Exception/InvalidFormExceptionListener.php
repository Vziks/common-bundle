<?php

namespace ADW\CommonBundle\EventListener\Exception;

use ADW\CommonBundle\Exception\InvalidFormException;
use ADW\CommonBundle\Exception\ValidationViolationException;
use ADW\CommonBundle\Validation\ViolationConverter;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Validator\ViolationMapper\ViolationMapper;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class InvalidFormExceptionListener
 *
 * @author Artur Vesker
 */
class InvalidFormExceptionListener
{

    /**
     * {@inheritdoc}
     */
    public function onException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (!$exception instanceof InvalidFormException) {
            return;
        }

        $form = $exception->getForm();

        if (!$form->isSubmitted()) {
            throw new BadRequestHttpException('Form empty or not handled');
        }

        /**
         * @var FormInterface $child
         */
        foreach ($form as $child) {
            if (!$child->isSubmitted() && $child->getConfig()->getType()->getInnerType() instanceof CheckboxType) {
                $child->submit(false);
            }
        }

        if ($extraViolationList = $exception->getExtraViolationList()) {
            $this->attachExtraViolations($extraViolationList, $form);
        }

        $errors = $form->getErrors(true);

        if (!$errors->count() > 0 && !$extraViolationList) {
/*            if ($extraViolationList) {
                throw new ValidationViolationException($extraViolationList, 'validation_error');
            }*/
            throw new \InvalidArgumentException('This form is valid');
        }



        $normalized = [];

        foreach ($errors as $error) {
            $normalized[] = [
                'name' => $error->getOrigin()->createView()->vars['full_name'] ?: null, //TODO: refactor
                'message' => $error->getMessage()
            ];
        }

        $event->setResponse(new JsonResponse(
            [
                'code' => 'invalid_form',
                'message' => $exception->getMessage(),
                'fields' => $normalized
            ],
            400
        ));
    }

    /**
     * @param ConstraintViolationListInterface $violationList
     * @param FormInterface $form
     */
    protected function attachExtraViolations(ConstraintViolationListInterface $violationList, FormInterface $form)
    {
        $mapper = new ViolationMapper();
        $converter = new ViolationConverter();

        foreach ($violationList as $violation) {
            $mapper->mapViolation($converter->toFormFormat($violation, $form), $form, true);
        }
    }

}