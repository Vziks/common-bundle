<?php

namespace ADW\CommonBundle;

use DeepCopy\DeepCopy;

/**
 * Class Cloner
 *
 * @package ADW\CommonBundle
 * @author Artur Vesker
 */
class Cloner
{

    /**
     * @var DeepCopy
     */
    static $deepCopyInstance;

    /**
     * @param $subject
     * @return mixed
     */
    public static function cloneIt($subject)
    {
        if (!self::$deepCopyInstance) {
            self::$deepCopyInstance = new DeepCopy();
            self::$deepCopyInstance->skipUncloneable(true);
        }

        return self::$deepCopyInstance->copy($subject);
    }

    private function __construct() {}
    private function __clone(){}
    private function __wakeup(){}

}