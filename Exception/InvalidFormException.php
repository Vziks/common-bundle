<?php

namespace ADW\CommonBundle\Exception;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class InvalidFormException.
 *
 * @author Artur Vesker
 */
class InvalidFormException extends \RuntimeException
{

    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var ConstraintViolationListInterface
     */
    protected $extraViolationList;

    /**
     * @param FormInterface $form
     * @param ConstraintViolationListInterface $extraViolationList
     */
    public function __construct(FormInterface $form, ConstraintViolationListInterface $extraViolationList = null)
    {
        $this->form = $form;
        $this->extraViolationList = $extraViolationList;

        $this->code = 'validation_error';

        parent::__construct('Validation Error');
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getExtraViolationList()
    {
        return $this->extraViolationList;
    }
}
