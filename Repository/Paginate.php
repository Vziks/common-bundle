<?php

namespace ADW\CommonBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class Paginate
 *
 * @package ADW\CommonBundle\Repository
 * @author Artur Vesker
 */
trait Paginate
{

    /**
     * @return EntityManagerInterface
     */
    abstract function getEntityManager();



    public function findByWithPagination(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $this->getEntityManager()->getUnitOfWork()->getEntityPersister()
    }

}