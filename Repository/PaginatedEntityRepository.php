<?php

namespace ADW\CommonBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Knp\Component\Pager\PaginatorInterface;

/**
 * Class PaginatedEntityRepository
 *
 * @package ADW\CommonBundle\Repository
 * @author Artur Vesker
 */
class PaginatedEntityRepository extends EntityRepository
{

    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    /**
     * @param PaginatorInterface $paginator
     * @param $em
     * @param Mapping\ClassMetadata $class
     */
    public function __construct(PaginatorInterface $paginator, $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
        $this->paginator = $paginator;
        $this->getEntityName()
    }

    use Paginate;
}