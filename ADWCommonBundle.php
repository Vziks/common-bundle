<?php

namespace ADW\CommonBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ADWCommonBundle
 *
 * @author Artur Vesker
 */
class ADWCommonBundle extends Bundle
{

}
