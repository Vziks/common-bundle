<?php

namespace ADW\CommonBundle\Controller;

use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;

/**
 * Class Controller
 *
 * @package ADW\CommonBundle\Controller
 * @author Artur Vesker
 */
class Controller extends BaseController
{

    /**
     * @inheritdoc
     */
    public function createForm($type = FormType::class, $data = null, array $options = array()): FormInterface
    {
        return $this->container->get('form.factory')->createNamed(null, $type, $data, array_merge(['csrf_protection' => false], $options));
    }

    /**
     * @inheritdoc
     */
    public function createPostForm($type = FormType::class, $data = null, array $options = array()): FormInterface
    {
        return $this->createForm($type, $data, array_merge($options, ['method' => 'POST']));
    }

    /**
     * @inheritdoc
     */
    public function createGetForm($type = FormType::class, $data = null, array $options = array()): FormInterface
    {
        return $this->createForm($type, $data, array_merge($options, ['method' => 'GET']));
    }


    /**
     * @inheritdoc
     */
    public function createPatchForm($type = FormType::class, $data = null, array $options = array()): FormInterface
    {
        return $this->createForm($type, $data, array_merge($options, ['method' => 'PATCH']));
    }

    /**
     * @return bool
     */
    public function isProdEnvironment()
    {
        return $this->get('kernel')->getEnvironment() === 'prod';
    }

    /**
     * @param $name
     * @param $data
     * @return $this|\ADW\JsContextBundle\JsContextInterface
     */
    public function injectJsContext($name, $data, SerializationContext $context = null)
    {
        return $this->get('js_context')->addData($name, $data, $context);
    }

    /**
     * @param $data
     * @param int $page
     * @param int $limit
     * @param array $options
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    protected function paginate($data, $page = 1, $limit = 10, array $options = [])
    {
        return $this->get('knp_paginator')->paginate($data, $page, $limit, $options);
    }

    /**
     * @param string $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return string
     */
    protected function trans($id, array $parameters = array(), $domain = null, $locale = null)
    {
        return $this->get('translator')->trans($id, $parameters, $domain, $locale);
    }

    /**
     * @param null $data
     * @param null $template
     * @param array $templateData
     * @param null $statusCode
     * @return View
     */
    protected function view($data = null, $template = null, array $templateData = [], $statusCode = null)
    {
        return View::create($data, $statusCode)->setTemplate($template)->setTemplateData($templateData);
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        return $this->isGranted('ROLE_USER');
    }

}