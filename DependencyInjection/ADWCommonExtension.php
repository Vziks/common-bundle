<?php

namespace ADW\CommonBundle\DependencyInjection;

use JMS\Serializer\SerializerBuilder;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sonata\MediaBundle\Model\Media;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ADWCommonExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if (isset($config['logger'])) {
            $this->handleLoggerSection($config['logger'], $container);
        }

        if (isset($config['response_headers'])) {
            $this->handleResponseHeadersSection($config['response_headers'], $container);
        }

        if (!empty($config['doctrine_table_prefix'])) {
            $tablePrefixSubscriber = new Definition('ADW\CommonBundle\EventListener\DoctrinePrefixSubscriber', [$config['doctrine_table_prefix']]);
            $tablePrefixSubscriber->addTag('doctrine.event_subscriber');
            $container->setDefinition('common.doctrine_prefix_subscriber', $tablePrefixSubscriber);
        }

        if (class_exists(Media::class)) {
            $sonataMediaSerializationHandlerDefinition = new Definition('ADW\CommonBundle\Serializer\Handler\SonataMediaHandler', [new Reference('sonata.media.pool')]);
            $sonataMediaSerializationHandlerDefinition->addTag('jms_serializer.subscribing_handler');
            $container->setDefinition('common.sonata_media_serialization_handler', $sonataMediaSerializationHandlerDefinition);
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * @param $config
     * @param ContainerBuilder $container
     */
    protected function handleResponseHeadersSection($config, ContainerBuilder $container)
    {
        $headersListenerDefinition = new Definition('ADW\CommonBundle\EventListener\HeadersListener', [$config]);
        $headersListenerDefinition->addTag('kernel.event_listener', ['event' => KernelEvents::RESPONSE, 'method' => 'onResponse']);
        $container->setDefinition('common.response_headers_listener', $headersListenerDefinition);
    }

    /**
     * @param $config
     * @param ContainerBuilder $container
     */
    protected function handleLoggerSection($config, ContainerBuilder $container)
    {
        if (isset($config['rollbar'])) {

            $rollbarConfig = $config['rollbar'];

            $rollbarOptions = $rollbarConfig['config'];
            $rollbarOptions['access_token'] = $rollbarConfig['token'];

            if (!$rollbarConfig['person_provider']) {
                $id = 'common.logger.rollbar_person_provider';
                $personProviderDefinition = new Definition('ADW\CommonBundle\Logger\RollbarPersonProvider', [new Reference('security.token_storage')]);
                $container->setDefinition($id, $personProviderDefinition);
            } else {
                $id = $rollbarConfig['person_provider'];
            }

            $rollbarOptions['person_fn'] = new Reference($id);

            $rollbarDefinition = new Definition('RollbarNotifier', [$rollbarOptions]);
            $rollbarDefinition->setPublic(false);

            $container->setDefinition('common.logger.rollbar', $rollbarDefinition);
        }
    }
}
