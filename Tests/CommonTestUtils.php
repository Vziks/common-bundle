<?php

namespace ADW\CommonBundle\Tests;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class CommonTestUtils
 *
 * @package ADW\CommonBundle\Tests
 * @author Artur Vesker
 */
class CommonTestUtils
{


    /**
     * @param $path
     * @param string $context
     * @param string $provider
     * @param string $mediaClass
     */
    public static function createMedia($path, $context = 'default', $provider = 'sonata.media.provider.image', $mediaClass = 'Application\Sonata\MediaBundle\Entity\Media')
    {
        $media = new $mediaClass();
        $media->setBinaryContent(new UploadedFile(
            $path,
            pathinfo($path)['basename'],
            mime_content_type($path),
            null,
            null,
            true
        ));
        $media->setContext($context);
        $media->setProviderName($provider);

        return $media;
    }

}